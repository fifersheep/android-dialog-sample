package uk.co.wanderingalbatross.example.dialogs

import android.app.Dialog
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fragment_my_dialog.*

class MyDialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = activity?.layoutInflater?.inflate(R.layout.fragment_my_dialog, null)
        return AlertDialog.Builder(context!!)
            .setTitle("Alert Dialog")
            .setView(view)
            .setPositiveButton("Go") { a, b ->
                val dialogListener = activity as DialogListener
                dialogListener.onFinishEditDialog(view?.findViewById<EditText>(R.id.input)?.text.toString())
                a.dismiss()
            }
            .setNegativeButton("Cancel") { _, _ -> dismiss() }
            .create()
    }

    interface DialogListener {
        fun onFinishEditDialog(inputText:String)
    }
}